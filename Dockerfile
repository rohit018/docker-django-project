FROM python:3.7.4-alpine
MAINTAINER rohitpatil@nimapinfotech.com

ENV PYTHONUNBUFFERED 1
ENV http_proxy http://proxy-chain.xxx.com:911/
ENV https_proxy http://proxy-chain.xxx.com:912/

COPY ./requirements.txt /requirements.txt   
RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./app /app

RUN adduser -D user
USER user

